export class Ingredient {
	/*We don't need to declare name and amount here like normal.
	
	Normally we would do:
	public name: string;
	public amount: number;
	Then in the constructor assign them like usual.

	Typescript lets us use this shortcut. It will automatically
	give us properties with the names we specify as argument names,
	and automatically assign the values in the constructor.*/
	constructor(public name: string, public amount: number) {}
}